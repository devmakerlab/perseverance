<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body style="font-family: 'Roboto', sans-serif;" class="antialiased bg-space">
        <div class="flex content-center flex-wrap min-h-screen">
            <div class="w-1/2 mx-auto p-4 bg-blue-500 rounded shadow-2xl">
                <div class="text-center text-xl font-bold text-white">
                    <p class="mb-4">
                        Perseverance vous souhaite la bienvenue sur la chaine > <a href="https://twitch.tv/devmaker_tv" class="underline hover:text-indigo-800">DevMaker</a> <
                    </p>
                    <img src="/images/selfie.png" alt="selfie" class="w-full"/>
                </div>
            </div>
        </div>
    </body>
</html>
